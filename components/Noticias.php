<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class Noticias extends widget
{
    public $titulo;
    
    public function init()
    {
        parent::init();
        echo '<div class="panel panel-default">';
        echo '<div class="panel-heading">';
        echo Html::tag("$this->titulo",['class'=>"panel-title"]);
        echo '</div>';
        echo '<div class="panel-body">';
    }
    
    public function run(){
        echo '</div></div>';
    }
}

