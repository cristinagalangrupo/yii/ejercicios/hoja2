DROP DATABASE IF EXISTS h2datos;
CREATE DATABASE h2datos;
USE h2datos;

CREATE TABLE entradas(
  id int(11) NOT NULL AUTO_INCREMENT,
  texto varchar(255) DEFAULT NULL,
  PRIMARY KEY(id)
  );

CREATE TABLE fotos (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255),
  contenido blob,
  alternativo varchar(255),
  PRIMARY KEY(id)
  );
INSERT INTO entradas(texto) VALUES ('Texo 1'), ('Texo 2'), ('Texo 3'), ('Texo 4');
